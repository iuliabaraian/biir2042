package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import evaluator.util.InputValidation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintWriter;

import static org.junit.Assert.*;

public class AppControllerTest {

    private String file;
    private IntrebariRepository intrebariRepository;
    private AppController appController;
    private InputValidation inputValidation;


    @Before
    public void setUp() throws Exception {
        file = "test.txt";
        this.intrebariRepository = new IntrebariRepository(file);
        this.inputValidation = new InputValidation();
        this.appController = new AppController(intrebariRepository, inputValidation);

    }

    @After
    public void tearDown() throws Exception {
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @Test
    public void tc1_ecp() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Celule");

        Intrebare added = appController.addNewIntrebare(intrebare);
        assertEquals(added.getEnunt(), intrebare.getEnunt());
        assertEquals(added.getVarianta1(), intrebare.getVarianta1());
        assertEquals(added.getVarianta2(), intrebare.getVarianta2());
        assertEquals(added.getVarianta3(), intrebare.getVarianta3());
        assertEquals(added.getVariantaCorecta(), intrebare.getVariantaCorecta());
        assertEquals(added.getDomeniu(), intrebare.getDomeniu());

    }

    @Test(expected = evaluator.exception.InputValidationFailedException.class)
    public void tc3_ecp() throws InputValidationFailedException, DuplicateIntrebareException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "7", "Celule");

        Intrebare added = appController.addNewIntrebare(intrebare);

    }

    @Test(expected = evaluator.exception.InputValidationFailedException.class)
    public void tc5_ecp() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Celule");
        Intrebare added = appController.addNewIntrebare(intrebare);


    }

    @Test
    public void tc8_bva() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "3", "Celule");

        Intrebare added = appController.addNewIntrebare(intrebare);
        assertEquals(added.getEnunt(), intrebare.getEnunt());
        assertEquals(added.getVarianta1(), intrebare.getVarianta1());
        assertEquals(added.getVarianta2(), intrebare.getVarianta2());
        assertEquals(added.getVarianta3(), intrebare.getVarianta3());
        assertEquals(added.getVariantaCorecta(), intrebare.getVariantaCorecta());
        assertEquals(added.getDomeniu(), intrebare.getDomeniu());


    }
    @Test
    public void tc7_bva() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Celule");

        Intrebare added = appController.addNewIntrebare(intrebare);
        assertEquals(added.getEnunt(), intrebare.getEnunt());
        assertEquals(added.getVarianta1(), intrebare.getVarianta1());
        assertEquals(added.getVarianta2(), intrebare.getVarianta2());
        assertEquals(added.getVarianta3(), intrebare.getVarianta3());
        assertEquals(added.getVariantaCorecta(), intrebare.getVariantaCorecta());
        assertEquals(added.getDomeniu(), intrebare.getDomeniu());


    }

    @Test(expected = evaluator.exception.InputValidationFailedException.class)
    public void tc9_bva() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "4", "Celule");

        Intrebare added = appController.addNewIntrebare(intrebare);

    }

    @Test(expected = evaluator.exception.InputValidationFailedException.class)
    public void tc4_bva() throws DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare = new Intrebare("care e celula cea mai mult raspandita", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "3", "Celule");

        Intrebare added = appController.addNewIntrebare(intrebare);

    }
}