package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import evaluator.util.InputValidation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintWriter;

public class AddTestTest {

    private String file;
    private IntrebariRepository intrebariRepository;
    private AppController appController;
    private InputValidation inputValidation;


    @Before
    public void setUp() throws Exception {
        file = "test.txt";
        this.intrebariRepository = new IntrebariRepository(file);
        this.inputValidation = new InputValidation();
        this.appController = new AppController(intrebariRepository, inputValidation);

    }

    @After
    public void tearDown() throws Exception {
        PrintWriter writer = new PrintWriter(file);
        writer.print("");
        writer.close();
    }

    @Test(expected = evaluator.exception.NotAbleToCreateTestException.class)
    public void tc1() throws NotAbleToCreateTestException {

        appController.createNewTest();
    }

    @Test
    public void tc2() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Boli");
        Intrebare intrebare2 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Organe externe");
        Intrebare intrebare3 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Organe interne");
        Intrebare intrebare4 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Celule");
        Intrebare intrebare5 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Alimentatie sanatoasa");

        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
        appController.addNewIntrebare(intrebare5);
        appController.domenii.add("Celule");
        appController.domenii.add("Boli");
        appController.domenii.add("Alimentatie sanatoasa");
        appController.domenii.add("Organe interne");
        appController.domenii.add("Organe externe");
        appController.testIntrebari.add(intrebare1);
        appController.testIntrebari.add(intrebare2);
        appController.testIntrebari.add(intrebare3);
        appController.testIntrebari.add(intrebare4);
        appController.testIntrebari.add(intrebare5);

        appController.createNewTest();

    }

    @Test
    public void tc3() throws NotAbleToCreateTestException, DuplicateIntrebareException, InputValidationFailedException {
        Intrebare intrebare1 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Boli");
        Intrebare intrebare2 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Organe externe");
        Intrebare intrebare3 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Organe interne");
        Intrebare intrebare4 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "2", "Celule");
        Intrebare intrebare5 = new Intrebare("Care e celula cea mai mult raspandita?", "Raspuns unu.", "Raspuns doi.", "Raspuns trei.", "1", "Alimentatie sanatoasa");

        appController.addNewIntrebare(intrebare1);
        appController.addNewIntrebare(intrebare2);
        appController.addNewIntrebare(intrebare3);
        appController.addNewIntrebare(intrebare4);
        appController.addNewIntrebare(intrebare5);

        appController.createNewTest();

    }
}
