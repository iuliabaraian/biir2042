package evaluator.model;

import java.util.LinkedList;
import java.util.List;

public class Test {

    private List<Intrebare> intrebari;

    public Test() {
        intrebari = new LinkedList<Intrebare>();
    }

    public List<Intrebare> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<Intrebare> intrebari) {
        this.intrebari = intrebari;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (Intrebare i:intrebari
             ) {
            sb.append(i.getDomeniu()+"\n");
            sb.append(i.getEnunt()+"\n");
            sb.append("1.");
            sb.append(i.getVarianta1()+"\n");
            sb.append("2.");
            sb.append(i.getVarianta2()+"\n");
            sb.append("3.");
            sb.append(i.getVarianta3()+"\n");
            sb.append("\n");
        }
        return String.valueOf(sb);
    }
}
