package evaluator.controller;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import evaluator.util.InputValidation;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class AppController {

    List<Intrebare> testIntrebari;
    List<String> domenii;
    private IntrebariRepository intrebariRepository;
    private InputValidation inputValidation;

    public AppController(IntrebariRepository intrebariRepository, InputValidation inputValidation) {
        this.intrebariRepository = intrebariRepository;
        this.inputValidation = inputValidation;
        testIntrebari = new LinkedList<>();
        domenii = new LinkedList<>();
    }

    public Intrebare addNewIntrebare(Intrebare intrebare) throws DuplicateIntrebareException, InputValidationFailedException {

        try {
            intrebariRepository.exists(intrebare);
            inputValidation.validateIntrebare(intrebare);
            intrebariRepository.addIntrebare(intrebare);
        } catch (InputValidationFailedException ex) {
            throw new InputValidationFailedException(ex.getMessage());
        } catch (DuplicateIntrebareException ex) {
            throw new DuplicateIntrebareException(ex.getMessage());
        } catch (IOException e) {
            e.getMessage();
        }


        return intrebare;
    }

    public Test createNewTest() throws NotAbleToCreateTestException {

        Intrebare intrebare;
        Test test = new Test();

        if (intrebariRepository.getNumberOfDistinctDomains() < 5)
            throw new NotAbleToCreateTestException("Nu exista suficiente domenii pentru crearea unui test!(5)");

        while (testIntrebari.size() != 5) {
            intrebare = intrebariRepository.pickRandomIntrebare();

            if (!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())) {
                testIntrebari.add(intrebare);
                domenii.add(intrebare.getDomeniu());
            }

        }

        testIntrebari.clear();
        domenii.clear();
        test.setIntrebari(testIntrebari);
        return test;

    }

    public Statistica getStatistica() throws NotAbleToCreateStatisticsException {

        if (intrebariRepository.getIntrebari().isEmpty())
            throw new NotAbleToCreateStatisticsException("Repository-ul nu contine nicio intrebare!");

        Statistica statistica = new Statistica();
        Set<String> domains= intrebariRepository.getDistinctDomains();
        for (String domain: domains
             ) {
            Integer number= intrebariRepository.getNumberOfIntrebariByDomain(domain);
            statistica.add(domain,number);
        }

        return statistica;
    }

}
