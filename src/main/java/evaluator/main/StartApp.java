package evaluator.main;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;
import evaluator.util.InputValidation;
import evaluator.view.UserInterface;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//functionalitati
//F01.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//F02.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//F03.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

    private static final String file = "intrebari.txt";

    public static void main(String[] args) {

        //   BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        try {
            IntrebariRepository intrebariRepository = new IntrebariRepository(file);
            InputValidation inputValidation = new InputValidation();
            AppController appController = new AppController(intrebariRepository, inputValidation);
            UserInterface userInterface = new UserInterface(appController);
            userInterface.show();

      /*      boolean activ = true;
            Integer optiune = null;

            while (activ) {

                System.out.println("");
                System.out.println("1.Adauga intrebare");
                System.out.println("2.Creeaza test");
                System.out.println("3.Statistica");
                System.out.println("4.Exit");
                System.out.println("");

                optiune = Integer.valueOf(console.readLine());

                if (optiune == 1) {

                    System.out.println("Introduceti intrebarea : ");
                    String intrebare = console.readLine();
                    System.out.println("Introduceti textul raspunsului pentru varianata 1 : ");
                    String varianta1 = console.readLine();
                    System.out.println("Introduceti textul raspunsului pentru varianata 2 : ");
                    String varianta2 = console.readLine();
                    System.out.println("Introduceti textul raspunsului pentru varianata 3 : ");
                    String varianta3 = console.readLine();
                    System.out.println("Introduceti numarul raspunsului corect ( 1, 2 sau 3 ) : ");
                    String rspunsCorect = console.readLine();
                    System.out.println("Domeniile sunt Organe interne, Celule, Alimentatie sanatoasa, Organe externe, Boli. Introdu numele domeniului dorit : ");
                    String domeniu = console.readLine();
                    Intrebare intrebareIntreaga = new Intrebare(intrebare, varianta1, varianta2, varianta3, rspunsCorect, domeniu);
                    try {
                        appController.addNewIntrebare(intrebareIntreaga);
                    } catch (DuplicateIntrebareException e) {
                        System.out.println(e.getMessage());
                    } catch (InputValidationFailedException e) {
                        System.out.println(e.getMessage());
                    }
                } else if (optiune == 2) {

                    try {
                        Test test = appController.createNewTest();
                        System.out.println(test.toString());
                    } catch (NotAbleToCreateTestException e) {
                        System.out.println(e.getMessage());
                    }
                } else if (optiune == 3) {

                    Statistica statistica;
                    try {
                        statistica = appController.getStatistica();
                        System.out.println(statistica);
                    } catch (NotAbleToCreateStatisticsException e) {
                        System.out.println(e.getMessage());
                    }
                } else if (optiune == 4) {
                    activ = false;
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
